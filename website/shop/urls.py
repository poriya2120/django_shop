from django.urls import path
from django.urls import path,include
from . import views

app_name='shop'
urlpatterns = [
    
    path('',views.index,name='index'),
    path('product/<int:pk>/',views.product,name='product'),
]