from django.shortcuts import render,get_object_or_404

from . import models
# Create your views here.
def index(request):
    product_list=models.Product.objects.all()[:5]
    return render(request,'index.html',{'product_list':product_list})

def product(request,pk):
    product_detail=get_object_or_404(models.Product,id=pk)
    return render(request,'product.html',{'product_detail':product_detail})